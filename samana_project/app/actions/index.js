﻿/**
 * Created by Simas on 2017-05-12.
 */
export const submitPost = (post) => (dispatch, getState, { client }) => {
  client.post('/api/posts', post)
    .then(() => dispatch(getPostsList()))
};

export const getPostsList = () => (dispatch, getState, { client }) => {
  dispatch({type: 'GET_POSTS_LIST'});
  client.get('/api/posts')
    .then((posts) => {
      dispatch({type: 'GET_POSTS_LIST_SUCCESS', posts});
    })
    .catch(() => {
      dispatch({type: 'GET_POSTS_LIST_FAILURE'});
    })
};

export const updatePostsLayout = (request) => (dispatch, getState, { client }) => {
  client.put('/api/posts', request)
    .then(() => dispatch(getPostsList()))
};

export const updateHeaderImage = (id, image) => (dispatch, getState, { client }) => {
  client.put(`/api/posts/${id}/image`, image)
    .then(() => dispatch(getPostsList()))
};

export const deletePost = (id) => (dispatch, getState, { client }) => {
  client.delete(`/api/posts/${id}`)
    .then(() => dispatch(getPostsList()))
};

export const getPost = (id) => (dispatch, getState, { client }) => {
  dispatch({type: 'GET_POST'});
  client.get(`/api/posts/${id}`)
    .then((post) => {
      dispatch({type: 'GET_POST_SUCCESS', post});
    })
    .catch(() => {
      dispatch({type: 'GET_POST_FAILURE'});
    })
};

export const updateChunks = (id, request) => (dispatch, getState, { client }) => {
  client.put(`/api/posts/${id}`, request);
};

export const addChunk = (id, chunk) => (dispatch, getState, { client }) => {
  client.post(`/api/posts/${id}`, chunk)
    .then(dispatch(getPost(id)));
};

export const updateChunksLayout = (id, request) => (dispatch, getState, { client }) => {
  client.put(`/api/posts/${id}`, request);
};

export const deleteChunk = (id, cId) => (dispatch, getState, { client }) => {
  client.delete(`/api/posts/${id}/${cId}`)
    .then(dispatch(getPost(id)));
};

export const closePost = () => (dispatch, getState, { client }) => {
  dispatch({type: 'CLOSE_POST'});
};
