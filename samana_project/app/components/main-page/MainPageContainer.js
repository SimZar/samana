/**
 * Created by Simas on 2017-05-12.
 */
import React from "react";
import {Link} from 'react-router';
import {PageHeader} from 'react-bootstrap';


export default class MainPageContainer extends React.Component {
  render() {
    return (
      <div>
        <PageHeader>Samana</PageHeader>
        <Link to="posts"> Posts </Link>
        <Link to="postsEdit"> Edit </Link>
        {this.props.children}
      </div>
    );
  }
}
