﻿using System.Collections.Generic;

namespace samana_project.DataContracts.Responses
{
  public class PostResponse
  {
    public int Id { get; set; }
    public List<ChunkResponse> Chunks { get; set; }
  }
}
