/**
 * Created by Simas on 2017-05-12.
 */
import { Provider } from 'react-redux';
import store, { history } from './store';
import { render } from 'react-dom';
import { Router } from 'react-router';
import routes from './routes/router-config';
import React from 'react';
require('../node_modules/react-grid-layout/css/styles.css');
require('../node_modules/react-resizable/css/styles.css');
require('../node_modules/react-quill/dist/quill.snow.css');

render((
	<Provider store={store}><Router history={history} routes={routes} /></Provider>
), document.getElementById('app'));
