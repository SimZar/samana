/**
 * Created by Simas on 2017-05-12.
 */
import { createStore, compose, applyMiddleware } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';
import rootReducer from './reducers';
import thunk from 'redux-thunk';

import HttpClientWrapper from './components/http-client/HttpClientWrapper';

const defaultState = {
	posts: {}
};

const client = new HttpClientWrapper();

const enhancers = compose(
	applyMiddleware(thunk.withExtraArgument({ client })),
	window.devToolsExtension ? window.devToolsExtension() : f => f,
);

const store = createStore(rootReducer, defaultState, enhancers);

export const history = syncHistoryWithStore(browserHistory, store);

export default store;
