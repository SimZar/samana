/**
 * Created by Simas on 2017-06-12.
 */
import ReactGridLayout from 'react-grid-layout';
import React from 'react';
import { Image } from 'react-bootstrap';
import { Well, Modal } from 'react-bootstrap';

export default class Post extends React.Component{
  render(){
    let chunks = this.props.chunks ? this.props.chunks.map((chunk, index) =>
        (<div id={chunk.id} title={index} key={index} data-grid={{x: chunk.position.x, y: chunk.position.y, w: chunk.position.width, h: chunk.position.height, isResizable: true}} onContextMenu={(e) => {e.preventDefault(); this.setState({postSelected: e.currentTarget.title})}
        }>
          {!chunk.content.startsWith('data:image') ? <div title={index} dangerouslySetInnerHTML={{__html: chunk.content}} /> : <Image id={chunk.id} src={chunk.content} responsive title={index}/> }
        </div>)
      ) : [];
    return(
      <Modal bsSize="large" show={this.props.show} onHide={this.props.close}>
        <Modal.Header closeButton />
        <Modal.Body>
          <Well>
            {this.props.loading ? <h3>Loading...</h3> :
              <ReactGridLayout className="layout" cols={12} rowHeight={30} width={1200} ref="grid"
                               isResizable={false} isDraggable={false}>
                {chunks}
              </ReactGridLayout>
            }
          </Well>
        </Modal.Body>
      </Modal>
    );
  }
}
