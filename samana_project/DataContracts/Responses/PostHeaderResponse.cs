﻿using samana_project.Data.Entities;

namespace samana_project.DataContracts.Responses
{
  public class PostHeaderResponse
  {
    public int Id { get; set; }
    public string Image { get; set; }
    public Block Position { get; set; }
  }
}
