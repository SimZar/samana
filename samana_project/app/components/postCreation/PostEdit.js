/**
 * Created by Simas on 2017-06-11.
 */

import ReactGridLayout from 'react-grid-layout';
import React from 'react';
import { Image } from 'react-bootstrap';
import FileBase64 from 'react-file-base64';
import { Button, Thumbnail, Well, Modal, ButtonGroup } from 'react-bootstrap';
import TextEditor from './TextEditor';

export default class PostEdit extends React.Component{
  constructor(props){
    super(props);
    this.state = {inputValue: '', chunks: [], image: '', postSelected: -1};
  }

  componentWillReceiveProps(nextProps){
    this.setState({chunks: nextProps.chunks});
  }

  submitOutput(){
    let request = this.state.chunks.map((post, index) => {
      let content = post.content;
      let item = this.refs.grid.state.layout[index];
      return{
        content,
        position:{
          x: item.x,
          y: item.y,
          height: item.h,
          width: item.w
        }
      }
    });
    this.props.updateChunks(this.props.id, request);
    this.props.close();
  }

  uploadImage(image){
    this.setState({ image: image.base64 });
  }

  uploadChunkImage(image){
    this.setState({ chunkTmpImage: image.base64 });
  }

  deleteChunk(){
    this.setState({
      chunks: [
        ...this.state.chunks.slice(0, parseInt(this.state.postSelected)),
        ...this.state.chunks.slice(parseInt(this.state.postSelected) + 1)
      ],
      postSelected: -1
    });
  }

  render(){
    let chunks = this.state.chunks ? this.state.chunks.map((chunk, index) =>
        (<div id={chunk.id} title={index} key={index} data-grid={{x: chunk.position.x, y: chunk.position.y, w: chunk.position.width, h: chunk.position.height, isResizable: true}} onContextMenu={(e) => {e.preventDefault(); this.setState({postSelected: e.currentTarget.title})}
        }>
          {!chunk.content.startsWith('data:image') ? <div title={index} dangerouslySetInnerHTML={{__html: chunk.content}} /> : <Image id={chunk.id} src={chunk.content} responsive title={index}/> }
        </div>)
      ) : [];
    return(
      <Modal bsSize="large" show={this.props.show} onHide={this.props.close}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Post</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Well>
            <ReactGridLayout className="layout" cols={12} rowHeight={30} width={1200} ref="grid">
              {chunks}
            </ReactGridLayout>
          </Well>
        </Modal.Body>
        <Modal.Footer>
          <Well>
            <TextEditor value={this.state.inputValue} onChange={(val) => this.setState({inputValue: val})} />
            <Button onClick={() => this.setState({chunks: [...this.state.chunks, {position: {x: 0, y: 0, width: 2, height: 5}, content: this.state.inputValue}], inputValue: ''})}>Add with text</Button>
            <ButtonGroup>
              <span>
                {this.state.image?
                  <span>
                    <h3>Image preview</h3>
                    <Thumbnail src={this.state.image} alt="242x200">
                      <p>
                        <Button bsStyle="success" onClick={() => this.setState({chunks: [...this.state.chunks, {position: {x: 0, y: 0, width: 2, height: 5}, content: this.state.image}], image: null})}>Submit</Button>
                        <Button bsStyle="danger" onClick={() => this.setState({image: null})}>Cancel</Button>
                      </p>
                    </Thumbnail>
                  </span>
                  : <Button>
                    <FileBase64
                      multiple={ false }
                      onDone={ this.uploadImage.bind(this) } />
                  </Button>}
              </span>
            </ButtonGroup>
          </Well>
          { this.state.postSelected !== -1 ?
            <div>
              <Well>
                {
                  this.state.chunks[this.state.postSelected].content.startsWith('data:image')?
                    <div>
                      <h3>Preview</h3>
                      <Image src={this.state.chunkTmpImage ? this.state.chunkTmpImage : this.state.chunks[this.state.postSelected].content} responsive />
                    </div> :
                    <div>
                      <h3>Preview</h3>
                      <div dangerouslySetInnerHTML={{__html: this.state.chunks[this.state.postSelected].content }} />
                    </div>}
                <ButtonGroup>
                  <Button bsStyle="warning" onClick={() => this.deleteChunk()}>Delete Chunk</Button>
                  <Button bsStyle="danger" onClick={() => this.setState({chunkTmpImage: null, postSelected: -1, postId: -1})}>Cancel</Button>
                </ButtonGroup>
              </Well>
            </div>
            :
            ''
          }
          <ButtonGroup>
            <Button bsStyle="success" onClick={() => this.submitOutput()} >Save Changes</Button>
            <Button bsStyle="danger" onClick={this.props.close}>Cancel</Button>
          </ButtonGroup>
        </Modal.Footer>
      </Modal>
    );
  }
}
