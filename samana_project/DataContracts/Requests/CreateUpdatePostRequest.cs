﻿using Microsoft.AspNetCore.Http;
using samana_project.Data.Entities;
using System.Collections.Generic;

namespace samana_project.DataContracts.Requests
{
  public class CreateUpdatePostRequest
  {
    public Block Position { get; set; }
    public string Image { get; set; }
    public List<CreateUpdateChunkRequest> Chunks { get; set; }
  }
}
