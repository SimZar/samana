/**
 * Created by Simas on 2017-05-12.
 */
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import posts from './Posts';
import post from './Post';

const rootReducer = combineReducers({
  posts,
  post,
	routing: routerReducer
});

export default rootReducer;
