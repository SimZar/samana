﻿using Microsoft.EntityFrameworkCore;
using samana_project.Data.Entities;
using samana_project.DataContracts.Requests;
using samana_project.DataContracts.Responses;
using System.Collections.Generic;
using System.Linq;

namespace samana_project.Data
{
  public class PostsRepository
  {
    private readonly ApplicationDbContext _dbContext;

    public PostsRepository(ApplicationDbContext appDbContext)
    {
      _dbContext = appDbContext;
    }

    public List<PostHeaderResponse> GetAll()
    {
      var posts = _dbContext.Posts
        .Include(post => post.PostImage)
        .ToList();
      var result = new List<PostHeaderResponse>();
      posts.ForEach(post => result.Add(new PostHeaderResponse
      {
        Id = post.Id,
        Image = post.PostImage.Data,
        Position = new Block
        {
          X = post.X,
          Y = post.Y,
          Height = post.Height,
          Width = post.Width
        }
      }));
      return result;
    }

    public PostResponse GetById(int id)
    {
      var post = _dbContext.Posts
        .Include(p => p.Chunks)
        .Single(x => x.Id == id);

      var result = new PostResponse
      {
        Id = post.Id,
        Chunks = new List<ChunkResponse>()
      };
      post.Chunks.ForEach(chunk => result.Chunks.Add(new ChunkResponse
      {
        Id = chunk.Id,
        Content = chunk.Content,
        Position = new Block
        {
          X = chunk.X,
          Y = chunk.Y,
          Width = chunk.Width,
          Height = chunk.Height
        }
      }));

      return result;
    }

    public void Post(CreateUpdatePostRequest request)
    {
      var post = new Post
      {
        X = request.Position.X,
        Y = request.Position.Y,
        Height = request.Position.Height,
        Width = request.Position.Width,
        Chunks = new List<Chunk>(),
        PostImage = new PostImage { Data = request.Image }
      };

      request.Chunks.ForEach(chunk => post.Chunks.Add(new Chunk
      {
        X = chunk.Position.X,
        Y = chunk.Position.Y,
        Height = chunk.Position.Height,
        Width = chunk.Position.Width,
        Content = chunk.Content
      }));

      _dbContext.Posts.Add(post);
      _dbContext.SaveChanges();
    }

    public void UpdatePostsLayout(List<UpdateLayoutRequest> request)
    {
      request.ForEach(req =>
      {
        var post = _dbContext.Posts.Single(x => x.Id == req.Id);
        post.X = req.Position.X;
        post.Y = req.Position.Y;
        post.Height = req.Position.Height;
        post.Width = req.Position.Width;
      });
      _dbContext.SaveChanges();
    }

    public void UpdatePostImage(int id, Image image)
    {
      var post = _dbContext.Posts
        .Include(p => p.PostImage)
        .Single(x => x.Id == id);
      _dbContext.Remove(post.PostImage);
      post.PostImage = new PostImage { Data = image.Data };
      _dbContext.SaveChanges();
    }

    public void UpdateChunks(int id, List<CreateUpdateChunkRequest> request)
    {
      var post = _dbContext.Posts
        .Include(p => p.Chunks)
        .Single(x => x.Id == id);
      request.ForEach(chunk =>
      {
        if (!post.Chunks.Exists(c => EqualChunks(c, chunk)))
        {
          post.Chunks.Add(new Chunk
          {
            X = chunk.Position.X,
            Y = chunk.Position.Y,
            Height = chunk.Position.Height,
            Width = chunk.Position.Width,
            Content = chunk.Content
          });
        }
      });

      post.Chunks.RemoveAll(chunk => !request.Exists(c => EqualChunks(chunk, c))); //c.Position.X == chunk.X && c.Position.Y == chunk.Y));
      _dbContext.SaveChanges();
    }

    public void PostChunk(int pId, CreateUpdateChunkRequest request)
    {
      var post = _dbContext.Posts
        .Include(p => p.Chunks)
        .Single(p => p.Id == pId);

      var chunk = new Chunk
      {
        X = request.Position.X,
        Y = request.Position.Y,
        Height = request.Position.Height,
        Width = request.Position.Width,
        Content = request.Content // ,
      };

      post.Chunks.Add(chunk);
      _dbContext.SaveChanges();
    }

    public void UpdateChunk(int pId, int cId, CreateUpdateChunkRequest request)
    {
      var post = _dbContext.Posts
        .Include(p => p.Chunks)
        .Single(p => p.Id == pId);

      var chunk = post.Chunks.Single(c => c.Id == cId);
      chunk.Content = request.Content;
    }

    public void DeletePost(int id)
    {
      var post = _dbContext.Posts
        .Include(p => p.PostImage)
        .Include(p => p.Chunks)
        .Single(x => x.Id == id);
      _dbContext.Remove(post);
      _dbContext.SaveChanges();
    }

    public void DeleteChunk(int pId, int cId)
    {
      var post = _dbContext.Posts
        .Include(p => p.Chunks)
        .Single(p => p.Id == pId);

      post.Chunks.RemoveAll(c => c.Id == cId);
      _dbContext.SaveChanges();
    }

    private bool EqualChunks(Chunk a, CreateUpdateChunkRequest b)
    {
      return (a.X == b.Position.X
        && a.Y == b.Position.Y
        && a.Width == b.Position.Width
        && a.Height == b.Position.Height
        && a.Content == b.Content);
    }

  }
}
