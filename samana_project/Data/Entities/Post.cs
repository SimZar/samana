﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace samana_project.Data.Entities
{
  public class Post : Block
    {
    public int Id { get; set; }

    [Required]
    public List<Chunk> Chunks { get; set; }

    public PostImage PostImage { get; set; }
    }
}
