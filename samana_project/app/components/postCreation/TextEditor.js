/**
 * Created by Simas on 2017-06-12.
 */
import React from 'react';
import ReactQuill from 'react-quill';

const TextEditor = (props) =>{
  return (
    <ReactQuill theme="snow" value={props.value} onChange={(value) => props.onChange(value)} />
  )
};
export default TextEditor;


