/**
 * Created by Simas on 2017-06-10.
 */
export default (state = {}, action) => {
  switch (action.type) {
    case 'GET_POST':
      return {
        ...state,
        loading: true
      };
    case 'GET_POST_SUCCESS':
      return {
        ...state,
        loading: false,
        ...action.post
      };
    case 'GET_POST_FAILURE':
      return{
        ...state,
        loading: false
      };
    case 'CLOSE_POST':
      return{
        loading: false
      };
    default:
      return state;
  }
};
