﻿using Microsoft.AspNetCore.Http;
using samana_project.Data.Entities;

namespace samana_project.DataContracts.Requests
{
  public class CreateUpdateChunkRequest
  {
    public Block Position { get; set; }
    public string Content { get; set; }
  }
}
