﻿using samana_project.Data.Entities;

namespace samana_project.DataContracts.Responses
{
  public class ChunkResponse
    {
    public int Id { get; set; }
    public Block Position { get; set; }
    public string Content { get; set; }
    }
}
