﻿using System.ComponentModel.DataAnnotations;

namespace samana_project.Data.Entities
{
  public class Image
  {
    public int Id { get; set; }
    [Required]
    public string Data { get; set; }
  }
}
