﻿using samana_project.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace samana_project.DataContracts.Requests
{
    public class UpdateLayoutRequest
    {
    public int Id { get; set; }
    public Block Position { get; set; }
    }
}
