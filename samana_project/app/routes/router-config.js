/**
 * Created by Simas on 2017-05-12.
 */
import React from 'react';
import { Route, IndexRoute } from 'react-router';

import MainPageContainer from '../components/main-page/MainPageContainer';
import PostsListEdit from '../components/postCreation/PostsListEdit'
import PostsList from '../components/postView/PostsList';

const routes =
		(<Route path="/" component={MainPageContainer} >
			<IndexRoute component={PostsListEdit} />
      <Route path="postsEdit" component={PostsListEdit} />
      <Route path="posts" component={PostsList} />
		</Route>)
	;

export default routes;
