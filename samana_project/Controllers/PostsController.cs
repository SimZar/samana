﻿using Microsoft.AspNetCore.Mvc;
using samana_project.Data;
using samana_project.Data.Entities;
using samana_project.DataContracts.Requests;
using samana_project.DataContracts.Responses;
using System;
using System.Collections.Generic;

namespace samana_project.Controllers
{
  [Route("api/posts")]
  public class PostsController : Controller
  {
    private readonly PostsRepository _postsRepository;

    public PostsController(PostsRepository postsRepository)
    {
      _postsRepository = postsRepository;
    }

    [HttpGet]
    public List<PostHeaderResponse> GetAll()
    {
      return _postsRepository.GetAll();
    }

    [HttpGet("{id}")]
    public PostResponse GetById(int id)
    {
      return _postsRepository.GetById(id);
    }

    [HttpPost]
    public IActionResult Post([FromBody]CreateUpdatePostRequest request)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
     
      _postsRepository.Post(request);
      return Ok();
    }

    [HttpPut]
    public IActionResult Put([FromBody]List<UpdateLayoutRequest> request)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest();
      }
      try
      {
        _postsRepository.UpdatePostsLayout(request);
        return Ok();
      }
      catch(Exception e)
      {
        return NotFound();
      }
    }

    [HttpPut("{id}/image")]
    public IActionResult PutImage(int id, [FromBody]Image image)
    {
      try
      {
        _postsRepository.UpdatePostImage(id, image);
        return Ok();
      }
      catch (Exception e)
      {
        return NotFound();
      }
    }

    [HttpPut("{id}")]
    public IActionResult PutChunks(int id, [FromBody]List<CreateUpdateChunkRequest> request)
    {
      try
      {
        _postsRepository.UpdateChunks(id, request);
        return Ok();
      }
      catch (Exception e)
      {
        return NotFound();
      }
    }

    [HttpDelete("{id}")]
    public IActionResult DeletePost(int id)
    {
      try
      {
        _postsRepository.DeletePost(id);
        return Ok();
      }
      catch (Exception e)
      {
        return NotFound();
      }
    }

  }
}
