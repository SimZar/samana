﻿/**
 * Created by Simas on 2017-05-12.
 */
import Promise from 'bluebird';
import request from 'superagent';

export default class HttpClientWrapper {
	get(url) {
		return new Promise((resolve, reject) => {
			request
				.get(url)
				.set('Content-Type', 'application/json')
				.timeout({
					response: 5000,
					deadline: 60000,
				})
				.end((err, res) => {
					if (res.status >= 200 && res.status < 300) {
						resolve(res.body);
					} else {
						reject(Error('Something is not right.'));
					}
				});
		});
	}

	post(url, data) {
		return new Promise((resolve, reject) => {
			request
				.post(url)
				.set('Content-Type', 'application/json')
				.send(data)
				.timeout({
					response: 5000,
					deadline: 60000,
				})
				.end((err, res) => {
					if (res.status >= 200 && res.status < 300) {
						resolve();
          }
          else {
            reject(Error('Something is not right.'));
          }
        });
		});
	}

	put(url, data) {
		return new Promise((resolve, reject) => {
			request
				.put(url)
				.set('Content-Type', 'application/json')
				.send(data)
				.timeout({
					response: 5000,
					deadline: 60000,
				})
				.end((err, res) => {
					if (res.status === 204 || res.status === 200) {
						resolve();
					} else {
						reject(Error('Something is not right.'));
					}
				});
		});
	}

	delete(url, data) {
		return new Promise((resolve, reject) => {
			request
				.del(url)
				.set('Content-Type', 'application/json')
				.send(data)
				.timeout({
					response: 5000,
					deadline: 60000,
				})
				.end((err, res) => {
					if (res.status === 204 || res.status === 200) {
						resolve();
					} else {
						reject(Error('Something is not right.'));
					}
				});
		});
	}
}
