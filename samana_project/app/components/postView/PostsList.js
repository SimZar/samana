/**
 * Created by Simas on 2017-06-12.
 */
import ReactGridLayout from 'react-grid-layout';
import React from 'react';
import { Image } from 'react-bootstrap';
import { Well } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions/index';
import Post from './Post';


class PostsList extends React.Component{
  componentDidMount(){
    this.props.edit.getPostsList();
  }
  render(){
    let posts = this.props.posts.posts ? this.props.posts.posts.map((post, index) =>
        (<div key={post.id}
              data-grid={{x: post.position.x, y: post.position.y, w: post.position.width, h: post.position.height, isResizable: false}}
              onClick={
                (e) => {
                  this.props.edit.getPost(e.target.id);
                  this.setState({showModal: true})}
                }>
          <Image src={post.image} id={post.id} name={index} responsive/>
        </div>)
      ) : [];
    return (
      <div>
        <Well>
          {this.props.posts.loading ? <h1>loading...</h1> :
            <ReactGridLayout className="layout" cols={12} rowHeight={30} width={1200} ref="grid" isResizable={false} isDraggable={false}>
              {posts}
            </ReactGridLayout>
          }
        </Well>

        <Post
          id={this.props.post? this.props.post.id : 0}
          show={this.state && this.state.showModal}
          chunks={this.props.post.chunks}
          close={() => {this.setState({showModal: false}); this.props.edit.closePost()}}
          loading={this.props.post.loading}
        /> }
      </div>
    )
  }
};

const mapDispatchToProps = dispatch => ({ edit: bindActionCreators(actionCreators, dispatch)});
const mapStateToProps = state => ({ posts: state.posts, post: state.post });

export default connect(mapStateToProps, mapDispatchToProps)(PostsList);
