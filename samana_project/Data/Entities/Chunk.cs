﻿using System.Collections.Generic;

namespace samana_project.Data.Entities
{
  public class Chunk : Block
    {
    public int Id { get; set; }

    public string Content { get; set; }
    //public ChunkImage ChunkImage { get; set; }

    public int PostId { get; set; }
    public Post Post { get; set; }
    }
}
