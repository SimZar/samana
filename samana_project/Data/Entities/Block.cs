﻿using System.ComponentModel.DataAnnotations;

namespace samana_project.Data.Entities
{
  public class Block
    {
    [Required]
    public int X { get; set; }
    [Required]
    public int Y { get; set; }

    [Required]
    public int Height { get; set; }
    [Required]
    public int Width { get; set; }
    }
}
