﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using samana_project.Data;

namespace samana_project.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("samana_project.Data.Entities.Chunk", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content");

                    b.Property<int>("Height");

                    b.Property<int>("PostId");

                    b.Property<int>("Width");

                    b.Property<int>("X");

                    b.Property<int>("Y");

                    b.HasKey("Id");

                    b.HasIndex("PostId");

                    b.ToTable("Chunk");
                });

            modelBuilder.Entity("samana_project.Data.Entities.Post", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Height");

                    b.Property<int>("Width");

                    b.Property<int>("X");

                    b.Property<int>("Y");

                    b.HasKey("Id");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("samana_project.Data.Entities.PostImage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Data")
                        .IsRequired();

                    b.Property<int>("PostId");

                    b.HasKey("Id");

                    b.HasIndex("PostId")
                        .IsUnique();

                    b.ToTable("PostImage");
                });

            modelBuilder.Entity("samana_project.Data.Entities.Chunk", b =>
                {
                    b.HasOne("samana_project.Data.Entities.Post", "Post")
                        .WithMany("Chunks")
                        .HasForeignKey("PostId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("samana_project.Data.Entities.PostImage", b =>
                {
                    b.HasOne("samana_project.Data.Entities.Post", "Post")
                        .WithOne("PostImage")
                        .HasForeignKey("samana_project.Data.Entities.PostImage", "PostId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
