/**
 * Created by Simas on 2017-05-12.
 */
export default (state = {}, action) => {
  switch (action.type) {
    case 'GET_POSTS_LIST':
      return {
        ...state,
        loading: true
      };
    case 'GET_POSTS_LIST_SUCCESS':
      return {
        ...state,
        loading: false,
        posts: action.posts
      };
    case 'GET_POSTS_LIST_FAILURE':
      return{
        ...state,
        loading: false
      };
    default:
      return state;
  }
};
