/**
 * Created by Simas on 2017-06-10.
 */
import ReactGridLayout from 'react-grid-layout';
import React from 'react';
import { Image } from 'react-bootstrap';
import FileBase64 from 'react-file-base64';
import { Button, Thumbnail, Well, ButtonGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions/index';
import PostEdit from './PostEdit';


class PostsListEdit extends React.Component{
  constructor(props){
    super(props);
    this.state = { postSelected: -1 };
  }

 componentDidMount(){
   this.props.edit.getPostsList();
 }

  uploadHeaderImage(image){
    this.setState({ tmpHeaderImage: image[0].base64 });
  }

  uploadPostImage(image){
    this.setState({ tmpPostImage: image[0].base64 });
  }

  updateHeaderImage(){
    this.props.edit.updateHeaderImage(this.state.postId, {id: -1, data: this.state.tmpPostImage});
    this.setState({tmpPostImage: null, postSelected: -1, postId: -1});
  }

  deletePost(){
    this.props.edit.deletePost(this.state.postId);
    this.setState({tmpPostImage: null, postSelected: -1, postId: -1});
  }

  updateLayout(){
    let newLayout = this.refs.grid.state.layout.map((post) =>{
      return {
        id: post.i,
        position:{
          x: post.x,
          y: post.y,
          height: post.h,
          width: post.w
        }
      }
    });
    this.props.edit.updatePostsLayout(newLayout);
  }

  submitPost(){
    let request = {};
    request.chunks = [];
    request.image = this.state.tmpHeaderImage;
    request.position = {
      x: 0,
      y: 1,
      width: 10,
      height: 11
    };
    this.setState({tmpHeaderImage: null});
    this.props.edit.submitPost(request);
  }
  render(){
    let posts = this.props.posts.posts ? this.props.posts.posts.map((post, index) =>
        (<div key={post.id}
              data-grid={{x: post.position.x, y: post.position.y, w: post.position.width, h: post.position.height, isResizable: true}}
              onContextMenu={
                (e) => {
                  e.preventDefault();
                  this.setState({postSelected: e.target.name, postId: e.target.id, tmpPostImage: this.props.posts.posts[e.target.name].image})
                }} >
          <Image src={post.image} id={post.id} name={index} responsive/>
        </div>)
      ) : [];
    return (
      <div>
        <Well>
          {this.props.posts.loading ? <h1>loading...</h1> :
            <ReactGridLayout className="layout" cols={12} rowHeight={30} width={1200} ref="grid">
              {posts}
            </ReactGridLayout>
          }
        </Well>
        <div>
          <span>
            {this.state.tmpHeaderImage ?
              <span>
                <h3>Image preview</h3>
                <Thumbnail src={this.state.tmpHeaderImage} alt="242x200">
                  <ButtonGroup>
                    <Button bsStyle="success" onClick={() => this.submitPost()}>Submit</Button>
                    <Button bsStyle="danger" onClick={() => this.setState({tmpHeaderImage: null})}>Cancel</Button>
                  </ButtonGroup>
                </Thumbnail>
              </span>
              : <Button>
                Upload posts header picture to create a new post
                <FileBase64
                  multiple={ true }
                  onDone={ this.uploadHeaderImage.bind(this) }/>
              </Button>}
          </span>
        </div>
        { this.state.postSelected !== -1 ?
          <div>
            <span>
              <span>
                <h3>Image preview</h3>
                <Thumbnail src={this.state.tmpPostImage ? this.state.tmpPostImage : this.props.posts.posts[this.state.postSelected].image} alt="242x200">
                  <Button>
                    Upload new posts header picture
                    <FileBase64
                      multiple={ true }
                      onDone={ this.uploadPostImage.bind(this) }/>
                  </Button>
                    <ButtonGroup>
                      <Button bsStyle="success" onClick={() => this.updateHeaderImage()}>Update Image</Button>
                      <Button bsStyle="info" onClick={() => {this.props.edit.getPost(this.state.postId); this.setState({showModal: true})}}>Edit Post</Button>
                      <Button bsStyle="warning" onClick={() => this.deletePost()}>Delete Post</Button>
                      <Button bsStyle="danger" onClick={() => this.setState({tmpPostImage: null, postSelected: -1, postId: -1})}>Cancel</Button>
                    </ButtonGroup>
                </Thumbnail>
              </span>
            </span>
          </div>
          :
          ''
        }
        <div>
          <Button bsStyle="success" onClick={() => this.updateLayout()}>Update Layout</Button>
        </div>
        { this.state.showModal &&
        <PostEdit
          id={this.state.postId}
          show={this.state.showModal}
          chunks={this.props.post.chunks}
          close={() => {this.setState({showModal: false}); this.props.edit.closePost()}}
          updateChunks={(id, request) => this.props.edit.updateChunks(id, request)}
        /> }
      </div>
    )
  }
};

const mapDispatchToProps = dispatch => ({ edit: bindActionCreators(actionCreators, dispatch)});
const mapStateToProps = state => ({ posts: state.posts, post: state.post });

export default connect(mapStateToProps, mapDispatchToProps)(PostsListEdit);
