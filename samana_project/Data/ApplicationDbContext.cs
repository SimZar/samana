﻿using Microsoft.EntityFrameworkCore;
using samana_project.Data.Entities;

namespace samana_project.Data
{
  public class ApplicationDbContext : DbContext
  {
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }

    public DbSet<Post> Posts { get; set; }
  }
}



