﻿using Microsoft.AspNetCore.Mvc;

namespace samana_project.Controllers
{
  public class HomeController : Controller
  {
    public IActionResult Index()
    {
      return File("/dist/index.html", "text/html");
    }
  }
}
